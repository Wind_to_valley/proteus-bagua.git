#include <REGX51.H>
#include <intrins.h>
#define uint unsigned int
#define uchar unsigned char
#define v1 1
#define v2 2
#define v3 3
#define Slave_num 1

sbit Elock=P2^0;								//控制眼锁存
sbit Llock=P2^1;								//控制阴阳锁存
sbit DIN = P2^2;       
sbit LOAD = P2^3;      
sbit CLK = P2^4;   
uchar code yang[8]={0x87,0x0f,0x1e,0x3c,0x78,0xf0,0xe1,0xc3};
uchar code eye[8]={0xa5,0x4b,0x96,0x2d,0x5a,0xb4,0x69,0xd2};
uchar code RGB[12]={0x01,0x01,0x02,0x02,0x04,0x04,0x03,0x03,0x05,0x05,0x06,0x06};
uint Delay=888;
uchar i=1,M, S, CS, judge=1,MSCS[6],Fcircle=0;
uchar flag,N;
void delay(unsigned int num)		//延时函数
{
	unsigned x, y;
	for(x=num;x>0;x--)
		for(y=110;y>0;y--);
}


void Write_MAX7219(unsigned char Addr,unsigned char Data)//地址和数据写入(内部函数不可引用)
{
	/*地址和数据均为两位16进制数，如0xFF*/
	unsigned char Count;    //计数变量
	LOAD = 0;   //LOAD引脚拉低电平开始写入数据
	for(Count = 0;Count < 8;Count++)        //写地址
	{
			CLK = 0;
			Addr <<= 1;
			DIN = CY;       //通过移位方式将一位数据移至单片机状态字寄存器的CY位，然后送出
			CLK = 1;
			delay(1);
			CLK = 0;
	}
	for(Count = 0;Count < 8;Count++)        //写数据
	{
			CLK = 0;
			Data <<= 1;
			DIN = CY;       //通过移位方式将一位数据移至单片机状态字寄存器的CY位，然后送出
			CLK = 1;
			delay(1);
			CLK = 0;
	}
	LOAD = 1;   //LOAD引脚拉高电平锁定数据
}

void Init_MAX7219(unsigned char Light_Coding,unsigned char SEG_Coding_Mode,unsigned char SEG_DIG_Num)   //使用前调用设置MAX7219
{
	/*亮度（0x00~0x0F）,编码模式（0x00,0x01,0x0F,0xFF）,数码扫描管个数（0x00~0x07）*/
	Write_MAX7219(0x09,SEG_Coding_Mode);
	Write_MAX7219(0x0A,Light_Coding);
	Write_MAX7219(0x0B,SEG_DIG_Num);
	Write_MAX7219(0x0C,0x01);     
}

void circle()
{
	Llock=1;										//锁存器八个管脚控制16个灯
	P1=yang[i];
	Llock=0;
	Elock=1;
	P1=eye[i];
	Elock=0;
	i++;
	if(i==8)										//数组循环，阴阳相济
		i=0;
}


void distime()
{
	Write_MAX7219(6,0x0A);
	Write_MAX7219(3,0x0A);
	if(judge)
	{
		Write_MAX7219(8,CS%10);
		Write_MAX7219(7,CS/10);
		Write_MAX7219(5,S%10);
		Write_MAX7219(4,S/10);
		Write_MAX7219(2,M%10);
		Write_MAX7219(1,M/10);		//显示
	}
	else
	{
		Write_MAX7219(8,MSCS[5]);
		Write_MAX7219(7,MSCS[4]);
		Write_MAX7219(5,MSCS[3]);
		Write_MAX7219(4,MSCS[2]);
		Write_MAX7219(2,MSCS[1]);
		Write_MAX7219(1,MSCS[0]);	//显示
	}
}

void main(void)
{
	P1 = 0xff;
//	SCON = 0xf0;    						 	//SM0/SM1/SM2/REN/TB8/RB8/TI/RI，串口模式3，SM2=1且允许接收
//	TMOD = 0x20;   								//定时器1为模式2，自动重装载
//																//波特率 = T1的溢出频率/32
//	TH1 = TL1 = 256 - 3;
//	TR1 = 1;  
	
	
	CS = S = M = 0;
	EA = 1;
	TH0 = (65536-50000)/256;
	TL0 = 65536-50000;
	TMOD = 0x51;
	ET0 = 1;
	TR0 = 0;								//定时器0
	
	IT0=1;
	EX0=1;									//中断0-暂停按键0
	
	IT1=1;
	EX1=1;									//中断1-停止归零按键1
	Init_MAX7219(0x0F,0xFF,0x07);
	
	ET1=1;
	TR1=1;
	TH1=TL1=0xff;						//定时器1改中断
	
	PT1=1;
	PT0=0;									//定时器1>定时器0

	P1=eye[2];
	Elock=1;
	Elock=0;
	P1=yang[2];
	Llock=1;
	Llock=0;

	while(1)
	{
//		if(RI)											//如果有接受
//		{
//			RI=0;											//软件清除接受标志
//			if(SM2==1)								//判断是广播还是点对点
//			{if(SBUF==Slave_num)SM2=0;}//广播到自己，进入点对点
//			else											//按第二次点对点的数据
//			{
//				switch(SBUF)						//读点对点1-4选择四种速度
//				{
//					case 1:Delay=110;break;
//					case 2:Delay=220;break;
//					case 3:Delay=440;break;
//					case 4:Delay=880;break;
//				}
//				SM2=1;									//回到广播地址阶段
//			}
//		}
//		delay(Delay);								//上面控制延时时间来控制速度
		
//		circle1(value(key_wat()));
		if(Fcircle)
			circle();
		distime();
	}
}

void Timer0(void) interrupt 1		//计时0
{
	TH0 = (65536-10000)/256;
	TL0 = 65536-10000;


	CS++;													//时间刷新
	if(CS>=100)
	{
		CS=0;
		S++;
		if(S>=60)
		{
			S=0;
			M++;
			if(M>=60)
			{M=0;}
		}
	}

}

void Button0(void) interrupt 0		//开始/暂停计时
{
	judge=1;
	TR0=~TR0;		
	Fcircle=~Fcircle;
}

void Button1(void) interrupt 2		//停止计时，归零
{
	char i;
	TR0=CS=S=M=0;
	for(i=0;i<6;i++)
	{MSCS[i]=0;}
	judge=1;
	Fcircle=0;
}
void Button2(void) interrupt 3		//分段计时，定时1改按键中断
{
	TH1=TL1=0xff;
//	judge=~judge;
	if(judge)
	{
		judge=0;
		MSCS[0]=M/10;
		MSCS[1]=M%10;
		MSCS[2]=S/10;
		MSCS[3]=S%10;
		MSCS[4]=CS/10;
		MSCS[5]=CS%10;
	}
	else
	{judge=1;}
}


