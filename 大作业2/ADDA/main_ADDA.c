#include <REGX52.H>
#include <intrins.h>									//空闲调用_nop_
#include <string.h>										//8管脚液晶屏调用strlen
#define uchar unsigned char						//缩写
#define uint unsigned int
#define ulong unsigned long
#define ok 1
#define error 0
#define LCD_addw 0x7c

#define Read 0x03											//EEPROM命令集
#define Write 0x02
#define Wren 0x06
#define Wrdi 0x04
#define Rdsr 0x05
#define Wrsr 0x01

sbit SCK=P2^4;												//串行时钟
sbit MOSI=P2^5;												//ROM输入
sbit MISO=P2^6;												//ROM输出
sbit CS=P2^7;													//ROM片选

sbit SCL=P2^0;												//IIC液晶屏
sbit SDA=P2^1;
sbit LCDRS=P2^2;											//8管脚液晶屏
sbit LCDEN=P2^3;

sbit DActrl=P3^3;												//DA开关
sbit MOctrl=P3^4;												//电机开关
sbit RVctrl=P3^5;												//RV2开关
sbit TNctrl=P3^1;												//负温感电阻开关
sbit VOctrl=P3^6;												//声音电阻开关
sbit LIctrl=P3^7;												//光感电阻开关
sbit MOTOR=P3^0;

ulong u32adv,j;													//adda共用存储
uchar dis_AD[16]={" AD_V =  .   mV"};	//AD显示框
uchar dis_DA[16]={" DA_V =  .   mV"};	//DA显示框
uchar code line1[16]={"press 1:DA;2:AD;"};
uchar code line2[16]={"      9:read"};
uchar code line3[16]={" 3:MO;4:VO;5:LI"};
uchar code line4[16]={" 6:T-;7:RV;8:RO"};
uchar code DA_choose[16]={"press 0~9 "};
uchar code DA_choose2[16]={"from 0.5 to 5mV"};
uchar code read_choose[16]={"1:last time DA"};
uchar code read_choose2[16]={"2:last time AD"};
uchar key_flag=0,key_2_flag=0,founc,key_2;								//键盘中断、DA标志
void delay(uint num)									//延时？ms
{
	uint x;
	uchar y;
	for(x=num;x>0;x--)
		for(y=120;y>0;y--);
}

void ddelay()													//延时5 us
{
}

void IIC_start()											//IIC-lcd1602函数群
{
	SDA=1;
	ddelay();
	SCL=1;
	ddelay();
	SDA=0;
	ddelay();
	SCL=0;
}

void IIC_stop()
{
	SDA=0;
	ddelay();
	SCL=1;
	ddelay();
	SDA=1;
	ddelay();
}

void IIC_ack()
{
	SDA=0;
	ddelay();
	SCL=1;
	ddelay();
	SCL=0;
}

void IIC_nack()
{
	SDA=1;
	ddelay();
	SCL=1;
	ddelay();
	SCL=0;
}
bit IIC_writebyte(uchar d)
{
	uchar i;
	bit ack;
	for(i=0;i<8;i++)
	{
		if((d<<i)&0x80)IIC_nack();
		else					 IIC_ack();
	}
	SDA=1;
	SCL=1;
	ack=SDA;
	SCL=0;
	return ack;
}
uchar IIC_readbyte()
{
	uchar i,d=0;
	SDA=1;
	for(i=0;i<8;i++)
	{
		SCL=1;
		ddelay();
		if(SDA) d+=(1<<(7-i));
		SCL=0;
	}
	return d;
}

void L_WriteCommand(uchar com)
{
	IIC_start();
	IIC_writebyte(LCD_addw);
	IIC_writebyte(0x80);
	IIC_writebyte(com);
	IIC_stop();
}
void L_WriteData(uchar dat)
{
	IIC_start();
	IIC_writebyte(LCD_addw);
	IIC_writebyte(0x40);
	IIC_writebyte(dat);
	IIC_stop();
}

void L_LCD1602_init()
{
	delay(10);
	L_WriteCommand(0x39);delay(5);
	L_WriteCommand(0x39);delay(5);
	L_WriteCommand(0x14);delay(5);
	L_WriteCommand(0x70);delay(5);
	L_WriteCommand(0x56);delay(5);
	L_WriteCommand(0x6c);delay(200);
	L_WriteCommand(0x0c);delay(5);
}

void L_Display_char(uchar row,uchar col,uchar dat)
{
	uchar a;
	if(row) a=0x80;
	if(row==2) a=0xc0;
	a+=col;
	L_WriteCommand(a);
	L_WriteData(dat);
}

void L_Display_string(uchar row,uchar col,uchar *p)
{
	uchar a,ip;
	if(row) a=0x80;
	if(row==2) a=0xc0;
	a+=col;
	L_WriteCommand(a);
	IIC_start();
	IIC_writebyte(LCD_addw);
	IIC_writebyte(0x40);
	ip=0;
	while(p[ip])
	{
		IIC_writebyte(p[ip]);
		ip++;
	}
	IIC_stop();
}

uchar PCF8591_AD()
{
	uchar adv;
	IIC_start();
	if(IIC_writebyte(0x90+1)) return error;
	adv=IIC_readbyte();IIC_ack();
	adv=IIC_readbyte();IIC_nack();
	IIC_stop();
	return adv;
}

uchar PCF8591_DA(uchar com,uint key_2)
{
	IIC_start();
	if(IIC_writebyte(0x90)) return error;
	if(IIC_writebyte(com))return error;
	if(IIC_writebyte(key_2))return error;
	IIC_stop();
	return ok;
}
void tran_AD()														//AD取值分解
{
	u32adv=PCF8591_AD();
	PCF8591_DA(0x40,(uchar)(u32adv&0xff));
	u32adv=(u32adv*5000)/255;
	dis_AD[8]=u32adv/1000+0x30;
	dis_AD[10]=(u32adv%1000)/100+0x30;
	dis_AD[11]=(u32adv%100)/10+0x30;
	dis_AD[12]=u32adv%10+0x30;
}
void tran_DA(uint DA_value)								//DA取值分解
{
	DA_value=DA_value*2;
	dis_DA[8]=DA_value/100+0x30;
	dis_DA[10]=(DA_value%100)/10+0x30;
	dis_DA[11]=DA_value%10+0x30;
}
void L_Clear()														//IIC液晶屏清屏
{
	L_Display_string(1,0,"                ");
	L_Display_string(2,0,"                ");
}
uchar key_wat()														//键盘检测
{
	uchar o=0,i=0,m;
	while(1)
	{
		if(key_2_flag&&key_flag==0){key_2_flag=0;break;}
		P1=(~(0x10<<i))|0x0f;			//P1的赋值与读取，测哪个键按下
		i++;
		if(i>3)i=0;
		_nop_();
		if((~P1)&0x0f)
		{
			m=P1;
			while(1)					//按下之后循环检测，测到松手代表按了一次
			{
				if((~P1&0x0f)==0)
				{
					o=1;
					break;
				}
			}
			if(o==1)break;
		}
	}
	return m;
}
void R_write_com(uchar com)               //第二块lcd1602写命令
{
	LCDRS=0;					//写命令模式
	P0 = com;					//要写的命令发到数据总线
	delay(5);					//延时等待数据稳定
	LCDEN=1;					//使能高脉冲
	delay(5);
	LCDEN=0;					//完成高脉冲
}
void R_init_lcd1602()                     //lcd1602初始化
{
//	dula=0;
//	wela=0;
	LCDEN=0;
	R_write_com(0x38);			//16×2显示，5×7点阵，8位数据接口
	R_write_com(0x0c);			//开显示，不显示光标
	R_write_com(0x06);			//写一个字符地址+1
	R_write_com(0x01);			//显示清0，数据指针清0
}

void R_write_data(uchar date)             //lcd1602写数据
{
   
	LCDRS=1;
	P0=date;
	delay(5);
	LCDEN=1;
	delay(5);
	LCDEN=0;
}
void R_lcd1602_string(uchar *p1,uchar *p2)//lcd1602启动
{
	uchar num;
	R_init_lcd1602();
	R_write_com(0x80);
	for(num=0;num<strlen(p1);num++)
	{
		R_write_data(p1[num]);
		delay(1);
	}
	R_write_com(0x80+0x40);
	for(num=0;num<strlen(p2);num++)
	{
		R_write_data(p2[num]);
		delay(1);
	}
}

uchar spi_io_byte(uchar d)								//ROM收发
{
	uchar i;
	uchar ret=0;
	SCK=1;
	for(i=0;i<8;i++)
	{
		SCK=0;
		_nop_();
		if(MISO) ret+=(1<<(7-i));
		MOSI=(bit)(d&0x80);
		_nop_();
		SCK=1;
		d<<=1;
	}
	return ret;
}

uchar SPI_eeprom_readsr()									//读取eeprom状态寄存器
{
	uchar byte=0;
	CS=0;																		//片选
	spi_io_byte(Rdsr);											//读状态命令
	byte=spi_io_byte(0xff);									//读一个字节
	CS=1;																		//取消片选
	return byte;
}

void SPI_eeprom_wait_busy()								//等busy清空
{
while((SPI_eeprom_readsr()&0x01)==0x01);
}

void SPI_eeprom_write_enable()						//SPI_flash写使能
{
	CS=0;
	spi_io_byte(Wren);
	CS=1;
}

void SPI_eeprom_read(uchar*pBuffer,uint ReadAddr,uchar NumByteRead)//指定地址读取指定长度数据
{
	uchar i;
	CS=0;																		//片选
	spi_io_byte(Read);											//读取命令
	spi_io_byte((uchar)(ReadAddr>>8));			//16位地址
	spi_io_byte((uchar)ReadAddr);
	for(i=0;i<NumByteRead;i++)
		pBuffer[i]=spi_io_byte(0xff);					//循环读数
	CS=1;																		//取消片选
}

void SPI_eeprom_write(uchar*pBuffer,uint WriteAddr,uchar NumByteWrite)//指定地址写N字节数据
{
	uchar i;
	SPI_eeprom_write_enable();							//写使能
	CS=0;																		//片选
	spi_io_byte(Write);											//写命令
	spi_io_byte((uchar)((WriteAddr)>>8));		//发送16位地址
	spi_io_byte((uchar)WriteAddr);
	for(i=0;i<NumByteWrite;i++)
		spi_io_byte(pBuffer[i]);							//循环写数
	CS=1;																		//取消片选
	SPI_eeprom_wait_busy();									//等写入结束
}

void main()
{
	uchar k,p,test1[16],test2[16];
	L_LCD1602_init();
	PCF8591_DA(0x40,0x80);
	EA=1;
	EX0=1;
	
	DActrl=MOctrl=MOTOR=RVctrl=TNctrl=VOctrl=LIctrl=0;	//DA断开;电机断开	
	PCF8591_DA(0x40,0);
	R_lcd1602_string(line1,line2);			//右屏显示功能选择提示
	L_Display_string(1,0,line3);				//左屏功能选择提示
	L_Display_string(2,0,line4);
	while(1)
	{
		switch(key_flag)
		{
			case 1: 
							if(founc!=1)founc=1;
							PCF8591_DA(0x40,j);
							key_2=key_wat();
							if(!key_flag)break;					//返回键盘，防止干扰
							switch(key_2)									//翻译成数值
							{
								case 0xeb:j=50;break;
								case 0xdb:j=75;break;
								case 0xbb:j=100;break;
								case 0xed:j=125;break;
								case 0xdd:j=150;break;
								case 0xbd:j=175;break;
								case 0xee:j=200;break;
								case 0xde:j=225;break;
								case 0xbe:j=250;break;
								case 0xd7:j=25;break;
								default:j=0;
							}
							tran_DA(j);
							L_Display_string(1,0,dis_DA);
							break;
			case 2:	
							if(founc!=2)founc=2;
							tran_AD();
							if(!key_flag)break;
							L_Display_string(1,0,dis_AD);
							break;
			case 9:
//							SPI_eeprom_read(test1,0,16);
//							SPI_eeprom_read(test1,17,16);
//							L_Display_string(1,0,dis_DA);
//							L_Display_string(2,0,dis_AD);
							key_2=key_wat();
							if(!key_flag)break;					//返回键盘，防止干扰
							switch(key_2)									//翻译成数值
							{
								case 0xeb:
													SPI_eeprom_read(test1,0,16);
													L_Display_string(1,0,dis_DA);
													break;
								case 0xdb:
													SPI_eeprom_read(test1,17,16);
													L_Display_string(2,0,dis_AD);
													break;
							}
							break;

			case 0: 
							p=key_wat();
							switch(p)									//翻译成数值
							{
								case 0xeb:
													L_Clear();
													DActrl=1;
													key_flag=1;
													key_2_flag=1;
//													L_Clear();
													R_lcd1602_string(DA_choose,DA_choose2);
													break;
								case 0xdb:
													L_Clear();
													key_flag=2;
													L_Clear();
													break;
								case 0xbb:
													RVctrl=TNctrl=VOctrl=LIctrl=0;
													L_Clear();
													MOctrl=~MOctrl;
													L_Display_string(2,2,"motor open");
													break;
								case 0xde:
													L_Clear();
													MOTOR=~MOTOR;
													L_Display_string(2,2,"press others");
													break;
								case 0xbd:
													RVctrl=TNctrl=VOctrl=LIctrl=0;
													L_Clear();
													TNctrl=~TNctrl;
													L_Display_string(2,2,"TN open");
													break;
								case 0xed:
													RVctrl=TNctrl=VOctrl=LIctrl=0;
													L_Clear();
													VOctrl=~VOctrl;
													L_Display_string(2,2,"VOICE open");
													break;
								case 0xdd:
													RVctrl=TNctrl=VOctrl=LIctrl=0;
													L_Clear();
													LIctrl=~LIctrl;
													L_Display_string(2,2,"LIGHT open");
													break;
								case 0xee:
													RVctrl=TNctrl=VOctrl=LIctrl=0;
													L_Clear();
													RVctrl=~RVctrl;
													L_Display_string(2,2,"RV2 open");
													break;
								case 0xbe:
													key_flag=9;
													key_2_flag=1;
													L_Clear();
													R_lcd1602_string(read_choose,read_choose2);
													break;
								case 0xd7:k=0;break;
								default:k=0;
							}
							break;
		}
	}
}

void Button0() interrupt 0								//返回键盘选择功能
{
	while(!P3_2);
	key_flag=0;
	PCF8591_DA(0x40,0);											//DA值清0
	DActrl=RVctrl=MOctrl=TNctrl=VOctrl=LIctrl=0;
	switch(founc)
	{
		case 1:SPI_eeprom_write(dis_DA,0,16);
		case 2:SPI_eeprom_write(dis_AD,17,16);
	}
	founc=0;
	L_Clear();
	R_lcd1602_string(line1,line2);			//右屏显示功能选择提示
	L_Display_string(1,0,line3);
	L_Display_string(2,0,line4);
}