#include <REGX52.H>
#include <intrins.h>
#include <iic.h>
#define LCD_ADDW    0x7C



void delayms(unsigned int t);
void WriteCommand(unsigned char com);
void WriteData(unsigned char dat);
void LCD1602_Init(void);
void display_char(unsigned char row,unsigned char col,unsigned char dat);
void display_string(unsigned char row,unsigned char col,unsigned char*p);

void delayms(unsigned int t)
{
    unsigned int i;
    unsigned char j;
    for(i=0;i<t;i++)
      for(j=0;j<200;j++);
}
void WriteCommand(unsigned char com)
{
     IIC_START();
     IIC_WriteByte(LCD_ADDW);  //iic 地址
     IIC_WriteByte(0x80);
     IIC_WriteByte(com);	
     IIC_STOP();
}
void WriteData(unsigned char dat)
{
    IIC_START();
    IIC_WriteByte(LCD_ADDW);  //iic 地址
    IIC_WriteByte(0x40);
    IIC_WriteByte(dat);
    IIC_STOP();
}
void LCD1602_Init(void)
{
    delayms(10);
    WriteCommand(0x39);delayms(5);
    WriteCommand(0x39);delayms(5);
    WriteCommand(0x14);delayms(5);
    WriteCommand(0x70);delayms(5);
    WriteCommand(0x56);delayms(5);
    WriteCommand(0x6c);delayms(200);
    WriteCommand(0x0c);delayms(5);
}



void display_char(unsigned char row,unsigned char col,unsigned char dat)
{   unsigned char a;
    if(row==1) a = 0x80;
    if(row==2) a = 0xc0;
    a = a+col;
    WriteCommand(a);
    WriteData(dat);
}
void display_string(unsigned char row,unsigned char col,unsigned char*p)
{   unsigned char a,ip;
    if(row==1) a = 0x80;
    if(row==2) a = 0xc0;
    a = a+col;//-1;
    WriteCommand(a);	    		//设置地址
    IIC_START();
    IIC_WriteByte(LCD_ADDW);  //iic 地址
    IIC_WriteByte(0x40);      //写数据命令
    ip = 0;
    while(p[ip]!=0)    {    IIC_WriteByte(p[ip]);	 ip++;      }
    IIC_STOP();
}
