#include <REGX52.H>
#include <intrins.h>
#define uchar unsigned char
#define uint unsigned int

uchar code Qian[8]={0x00,0x00,0xff,0x00,0xff,0x00,0xff,0x00};
uchar code Xun[8]={0x10,0x48,0x24,0x92,0x49,0x04,0x12,0x08};
uchar code Kan[8]={0x00,0x00,0xe7,0x00,0xff,0x00,0xe7,0x00};
uchar code Gen[8]={0x10,0x48,0x24,0x82,0x41,0x04,0x12,0x08};
uchar code Kun[8]={0x00,0x00,0xe7,0x00,0xe7,0x00,0xe7,0x00};
uchar code Zhen[8]={0x10,0x48,0x20,0x82,0x41,0x24,0x12,0x08};
uchar code Li[8]={0x00,0x00,0xff,0x00,0xe7,0x00,0xff,0x00};
uchar code Dui[8]={0x10,0x48,0x20,0x92,0x49,0x24,0x12,0x08};
uchar code Tian[22]="\r天行健  子以自强不息\r";
uchar code Di[22]="\r地势坤  子以厚德载物\r";
uchar code Feng[22]="\r随风巽  子以申命行事\r";
uchar code Lei[22]="\r渐雷震  子以恐惧修省\r";
uchar code Shui[22]="\r善若水  子以作事谋始\r";
uchar code Huo[22]="\r火同人  子以类族辨物\r";
uchar code Ze[22]="\r步泽履  子以辩民安志\r";
uchar code Shan[22]="\r艮山谦  子以裒多益寡\r";
uchar i=0,j=1,k=0;
uint l=0;
void ddelay(uint num)				//延时函数
{
	uint x,y;
	for(x=num;x>0;x--)
		for(y=45;y>0;y--);
}

void bagua_Q()
{

	P2=1<<i;
	P1=~Qian[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=3;}
}
void bagua_X()
{

	P2=1<<i;
	P1=~Xun[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=4;}
}
void bagua_K()
{

	P2=1<<i;
	P1=~Kan[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=5;}
}
void bagua_G()
{

	P2=1<<i;
	P1=~Gen[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=6;}
}
void bagua_KU()
{

	P2=1<<i;
	P1=~Kun[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=7;}
}
void bagua_Z()
{

	P2=1<<i;
	P1=~Zhen[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=8;}
}
void bagua_L()
{

	P2=1<<i;
	P1=~Li[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=1;}
}
void bagua_D()
{

	P2=1<<i;
	P1=~Dui[i];
	ddelay(1);
	i++;
	if(i>7){i=0;j=2;}
}

void bagua_circle()
{
	P1=0xff;
	P2=0x00;
	switch(j)
	{
		case 1:P0=0x01;
						bagua_Q();break;
		case 2:P0=0x02;
						bagua_X();break;
		case 3:P0=0x04;
						bagua_K();break;
		case 4:P0=0x08;
						bagua_G();break;
		case 5:P0=0x10;
						bagua_KU();break;
		case 6:P0=0x20;
						bagua_Z();break;
		case 7:P0=0x40;
						bagua_L();break;
		case 8:P0=0x80;
						bagua_D();break;
	}
}
void send_byte(uchar d)
{
	SBUF=d;
	while(!TI);
	TI=0;
}
void send_string(uchar*p)
{
	uchar pt=0;
	while(p[pt]!=0)
	{
		send_byte(p[pt]);
		pt++;
	}
}

void main(void)
{
	P2=0x00;
	EA=1;
	SCON=0x50;
	TMOD=0x20;
	IT0=IT1=1;
	EX0=EX1=1;
	TH1=TL1=256-3;
	TR1=1;
	PS=1;
	
	send_string("\r        乾   \r");
	send_string("      离  坎\r");
	send_string("        坤\r");

	
	while(1)
	{
		if(RI)
		{
			RI = 0;	
			switch(SBUF)
			{
				case '1':send_string("\r  天行健  子以自强不息\r");k=1;break;
				case '2':send_string("\r  随风巽  子以申命行事\r");k=2;break;
				case '3':send_string("\r  善若水  子以作事谋始\r");k=3;break;
				case '4':send_string("\r  艮山谦  子以裒多益寡\r");k=4;break;
				case '5':send_string("\r  地势坤  子以厚德载物\r");k=5;break;
				case '6':send_string("\r  渐雷震  子以恐惧修省\r");k=6;break;
				case '7':send_string("\r  火同人  子以类族辨物\r");k=7;break;
				case '8':send_string("\r  步泽履  子以辩民安志\r");k=8;break;
				case '0':	k=0;
									if(j==2)
									{
										j=1;
										
										send_string("\r        乾   \r");
										send_string("      离  坎\r");
										send_string("        坤\r");
									}
									else if(j==1) 
									{
										j=2;
										send_string("\r      兑  巽\r");
										send_string("\r");
										send_string("      震  艮\r");
									}break;
			}
		}
		P2=0x00;
		switch(k)
		{
			case 0:bagua_circle();break;
			case 1:P0=0x01;
							bagua_Q();break;
			case 2:P0=0x02;
							bagua_X();break;
			case 3:P0=0x04;
							bagua_K();break;
			case 4:P0=0x08;
							bagua_G();break;
			case 5:P0=0x10;
							bagua_KU();break;
			case 6:P0=0x20;
							bagua_Z();break;
			case 7:P0=0x40;
							bagua_L();break;
			case 8:P0=0x80;
							bagua_D();break;
		}
		if(!k)
		{
			l++;
			if(l>3300)
			{
				l=0;
				if(j==2)
				{
					j=1;
					
					send_string("\r        乾   \r");
					send_string("      离  坎\r");
					send_string("        坤\r");
				}
				else if(j==1) 
				{
					j=2;
					send_string("\r      兑  巽\r");
					send_string("\r");
					send_string("      震  艮\r");
				}
			}
		}
	}
}

void Button0(void) interrupt 0		//开始/暂停计时
{
	P0=0xff;
	P2=0x00;
	_nop_();
	P1=0xff;
	k++;
	if(k>8)k=0;
	switch(k)
	{
		case 1:send_string("\r        乾  \r");break;
		case 2:send_string("\r        巽  \r");break;
		case 3:send_string("\r        坎  \r");break;
		case 4:send_string("\r        艮  \r");break;
		case 5:send_string("\r        坤  \r");break;
		case 6:send_string("\r        震  \r");break;
		case 7:send_string("\r        离  \r");break;
		case 8:send_string("\r        兑  \r");break;
	}
}
void uart_isr() interrupt 4
{
	if(TI)
	{
		TI = 0;
	}
	else
	{
		P3_4=~P3_4;
		RI = 0;
	}
}