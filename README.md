﻿## proteus+keilC51制作八卦阵
# 前言

本文展示用keil c51 + proteus制作一个八卦阵的过程。

文末获取工程文件及其他资料，建议结合keil工程阅读本文。

## 效果

![八卦阵](https://img-blog.csdnimg.cn/f43e737f15bf47278c206ed9d8a7d4c6.gif#pic_center)

# 组成

![组成框图](https://img-blog.csdnimg.cn/img_convert/21699070fb726d042ed5eaf9642243d5.png)

## MCU

![AT89C52模型](https://img-blog.csdnimg.cn/img_convert/5c7264f581a3ca3684a1350a1459ac75.png)

受到proteus和笔者电脑性能的限制，stm32模型运行及其卡顿，所以此处使用51单片机，完成这个简单的任务。
为了整体的观赏性，避免八卦和阴阳鱼像干扰，用了两个相互独立的51芯片。

## 锁存器

![74HC573锁存器](https://img-blog.csdnimg.cn/img_convert/a3fb2e2d72685b68fef364fe6719dc22.png)

为了实现一组管脚控制多个LED和点阵，这里使用了锁存器，使用方法非常简单，OE置0，当LE脚输入高电平，Q\[0 7\]=D\[0 7\]，当LE脚输入低电平，Q\[0 7\]锁死。

## 显示

![LED模型](https://img-blog.csdnimg.cn/img_convert/ec98325ab686f1f7640d275ff859b514.png)

因为在proteus，所以将单片机引脚直接接LED正极，逻辑1点亮，实际上51单片机可能没有足够的驱动能力点亮LED，一般将引脚接为负极，正极接上拉电阻，逻辑0点亮，移植需要注意。

![8\*8点阵模型](https://img-blog.csdnimg.cn/img_convert/3c30940c4fb3f29e1694cff0535db9ab.png)

一共16个引脚，8个控制第n行，8个控制某行中的n点，具体操作可以自己实验尝试。
以上模型都可以在proteus找到并放置。

# 阴阳鱼

![八卦阵](https://img-blog.csdnimg.cn/img_convert/20b2ac133acb455ad0875a550389b38c.png)

按八卦阵的顺序摆好LED。
因为阴阳鱼眼睛和周围反颜色，所以将中间这一圈设为一组，内外两圈设为一组。

![锁存器连接](https://img-blog.csdnimg.cn/img_convert/7721c20d062e84adbc3148eaaf11c522.png)

新建51工程，添加预定义，这里选择P2.0,
P2.1连接两个LE脚，并定义数组和顺序号，方便后面的循环。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
sbit Elock=P2^0;        //控制眼锁存
sbit Llock=P2^1;        //控制阴阳锁存
uchar code yang[8]={0x87,0x0f,0x1e,0x3c,0x78,0xf0,0xe1,0xc3};
uchar code eye[8]={0xa5,0x4b,0x96,0x2d,0x5a,0xb4,0x69,0xd2};
uchar i=1;
```

手动添加延时函数，这里大概是1毫秒

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void delay(unsigned int num)    //延时函数
{
    unsigned x, y;
    for(x=num;x>0;x--)
    for(y=120;y>0;y--);
}
```

LED控制函数，按i的顺序读取两个数组，让i在0 7循环，就可以实现阴阳鱼在旋转的效果。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void circle()
{
    Llock=1;        //锁存器八个管脚控制16个灯
    P1=yang[i];
    Llock=0;
    Elock=1;
    P1=eye[i];
    Elock=0;
    i++;
    if(i==8)        //数组循环，阴阳相济
    i=0;
}
```

主函数中，先让LED全灭，然后开始循环。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void main(void)
{
    P1 = 0xff;  //先全灭
    
    P1=eye[2];  //开始位置，可以自己尝试其他位置
    Elock=1;
    Elock=0;
    P1=yang[2];
    Llock=1;
    Llock=0;
    
    while(1)
    {
        circle();
        delay(999);
    }
}
```

到此，八卦阵就做好了，点击开始仿真，可以显示循环效果。

# 八卦阵

![AT89C52](https://img-blog.csdnimg.cn/img_convert/315d62737f495bf66dd9aa4ea532f850.png)

8个点阵需要8个锁存器，此处选用P0作为8个锁存器的le控制，注意这款51单片机的P0需要接上拉电阻才可以输出信号。

![八卦阵对应锁存器](https://img-blog.csdnimg.cn/img_convert/edc21b071538589c106d21ac482b989c.png)

![八卦阵](https://img-blog.csdnimg.cn/img_convert/1d366ba27f2e32dc07fbfe127e9326b6.png)

这里使用P2口来控制点阵显示在哪一行，P1口控制显示在哪一点，循环到一个点阵时，其他点阵的锁存器都是关闭信号且锁住，然后控制显示内容。
定义八个数组和一些变量。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
uchar code Qian[8]={0x00,0x00,0xff,0x00,0xff,0x00,0xff,0x00};
uchar code Xun[8]={0x10,0x48,0x24,0x92,0x49,0x04,0x12,0x08};
uchar code Kan[8]={0x00,0x00,0xe7,0x00,0xff,0x00,0xe7,0x00};
uchar code Gen[8]={0x10,0x48,0x24,0x82,0x41,0x04,0x12,0x08};
uchar code Kun[8]={0x00,0x00,0xe7,0x00,0xe7,0x00,0xe7,0x00};
uchar code Zhen[8]={0x10,0x48,0x20,0x82,0x41,0x24,0x12,0x08};
uchar code Li[8]={0x00,0x00,0xff,0x00,0xe7,0x00,0xff,0x00};
uchar code Dui[8]={0x10,0x48,0x20,0x92,0x49,0x24,0x12,0x08};
uchar i=0,j=1;
uint l=0;
```

变量i作为行数的记号，j是下一个点阵的记号和分组点阵记号，l是在主函数中记录时间的变量。
受到proteus或硬件性能限制，同时八个点阵循环视觉效果非常差，所以设置的逻辑是四个对角点阵显示，过一段时间l之后轮到另外四个对角点阵显示。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void ddelay(uint num)               //延时函数
{
    uint x,y;
    for(x=num;x>0;x--)
    for(y=45;y>0;y--);
}
```

手动延时，当视觉效果不理想时可以试试更改y值。

下面是八个点阵对应控制函数中的一个，其他见工程，j代表下一个点阵的标号，在后面的switch中调用判断。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void bagua_Q()
{
    
    P2=1<<i;
    P1=~Qian[i];
    ddelay(1);
    i++;
    if(i>7){i=0;j=3;}
}
```

整体循环函数，每次先刷新后判断到哪一个点阵。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void bagua_circle()
{
    P1=0xff;
    P2=0x00;
    switch(j)
    {
        case 1:P0=0x01;
        bagua_Q();break;
        case 2:P0=0x02;
        bagua_X();break;
        case 3:P0=0x04;
        bagua_K();break;
        case 4:P0=0x08;
        bagua_G();break;
        case 5:P0=0x10;
        bagua_KU();break;
        case 6:P0=0x20;
        bagua_Z();break;
        case 7:P0=0x40;
        bagua_L();break;
        case 8:P0=0x80;
        bagua_D();break;
    }
}
```

主函数循环内调用bagua circle();循环显示八卦阵，同一时间只会看到\[乾 坎
坤 离\]或者\[巽 艮 震
兑\]四个卦象，同时l变量计时，超过对应值时切换一组，更改对应值可以更改单组显示时间。

```{.[ANSI]C language="[ANSI]C" title="main.c"}
void main(void)
{
    P2=0x00;
    ddelay(1);
    while(1)
    {
        
        P2=0x00;
        bagua_circle();
        
        l++;
        if(l>999)
        {
            l=0;
            if(j==2)
            {
                j=1;
            }
            else if(j==1) 
            {
                j=2;
            }
        }
    }
}
```

到此，太极八卦阵就做好啦，感兴趣的同学可以换上stm32等更快的单片机，设计PCB板，做出实物哦(●'◡'●)

![太极八卦阵](https://img-blog.csdnimg.cn/img_convert/a09f4263b38a6561a7cd9fa832a5aaa3.png)

# 下载链接

```{.[ANSI]C language="[ANSI]C" title="工程文件下载链接"}
链接：https://pan.baidu.com/s/1oXmYgVkh0gdzHd46MvwZ4Q 
提取码：tEGO 
复制这段内容后打开百度网盘手机App，操作更方便哦
```
单片机，设计PCB板，做出实物哦(●'◡'●)



# 下载链接

```{.[ANSI]C language="[ANSI]C" title="工程文件下载链接"}
链接：https://pan.baidu.com/s/1oXmYgVkh0gdzHd46MvwZ4Q 
提取码：tEGO 
复制这段内容后打开百度网盘手机App，操作更方便哦
```
